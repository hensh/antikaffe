package com.hensh.anticaffee.db;

import java.util.Date;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.hensh.anticaffee.db.AnticaffeeProviderMetaData.VisitsTableMetaData;
import com.hensh.anticaffee.model.Visit;

public class VisitsDataSource
{
	public final static int ActiveOnly = 0;
	public final static int ClosedOnly = 1;
	public final static int AllVisits = 2;
	
	private Cursor cursor;
	private Date startDate;
	private Date finishDate;
	private int mode;
	
	public int getMode()
	{
		return mode;
	}
	
	public Date getStartDate()
	{
		return startDate;
	}
	
	public Date getFinishDate()
	{
		return finishDate;
	}
	
	public VisitsDataSource(int mode, ContentResolver resolver)
	{
		this(null, null, mode, resolver);
	}
	
	public VisitsDataSource(Date startDate, int mode, ContentResolver resolver)
	{
		this(startDate, null, mode, resolver);
	}
	
	public VisitsDataSource(Date startDate, Date finishDate, int mode, ContentResolver resolver)
	{
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.mode = mode;
		
		String whereDate = "";
		boolean notFirst = false;
		if (startDate != null)
		{
			whereDate += AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_ENTER_DATE + " > " + startDate.getTime();
			notFirst = true; 
		}
		String modeCondition = "";
		switch (mode)
		{
			case ActiveOnly:
				modeCondition = AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE + " IS NULL";
				break;
				
			case ClosedOnly:
				modeCondition = AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE + " IS NOT NULL";
				break;
				
			case AllVisits:	
				break;
		}
		
		if (notFirst) whereDate += " AND ";
		whereDate += modeCondition;
		
		if (finishDate != null)
		{
			whereDate += " AND " + AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_ENTER_DATE + " < " + finishDate.getTime();
		}
		Uri uri = VisitsTableMetaData.CONTENT_URI;
		cursor = resolver.query(uri, AnticaffeeProviderMetaData.columns, whereDate, null, AnticaffeeProviderMetaData.VisitsTableMetaData.DEAFULT_SORT_ORDER);
		cursor.moveToFirst();
	}
	
	public int getCount()
	{
		return cursor.getCount();
	}
	
	public Date getEnterDate(int index)
	{
		cursor.moveToPosition(index);
		return new Date(cursor.getLong(3));
	}
	
	public Visit getVisit(int index)
	{
		if (!cursor.moveToPosition(index))
		{
			return null;
		}
		return DataStorage.readVisitFromCursor(cursor);
	}
	
	public long getVisitID(int index)
	{
		cursor.moveToPosition(index);
		return cursor.getLong(0);
	}
	
	public void close()
	{
		cursor.close();
	}
}

package com.hensh.anticaffee.db;

import java.util.Date;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.hensh.anticaffee.db.AnticaffeeProviderMetaData.VisitsTableMetaData;
import com.hensh.anticaffee.model.Visit;

public class DataStorage
{	
	public static int CostPerPersonUndefined = -1;
	private final Context context;
	
	public DataStorage(Context context)
	{
		this.context = context;
	}
	
	public boolean delete(Visit visit)
	{
		return delete(visit.getId());
	}
	
	public boolean delete(long visitId)
	{
		if (visitId == Visit.UndefinedID)
		{
			return false;
		}
		
		ContentResolver resolver = context.getContentResolver();
		Uri uri = VisitsTableMetaData.CONTENT_URI;
		Uri deleteUri = Uri.withAppendedPath(uri, VisitsTableMetaData.VISIT_BY_ID);
		deleteUri = Uri.withAppendedPath(deleteUri, String.valueOf(visitId));
		boolean result = resolver.delete(deleteUri, null, null) > 0;
		resolver.notifyChange(VisitsTableMetaData.CONTENT_URI, null);
		return result;
	}
	
	public Visit save(Visit visit)
	{
		ContentValues cv = new ContentValues();
		cv.put(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_CARD_NUMBER, visit.getCardNumber());
		cv.put(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_ENTER_DATE, visit.getEnterDate().getTime());
		cv.put(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_PERSON_COUNT, visit.getPersonsCount());
		cv.put(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_COST_PER_PERSON, visit.getCostPerPerson());
		if (visit.isActive())
		{
			cv.putNull(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE);
		}
		else
		{
			cv.put(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE, visit.getExitDate().getTime());
		}
		
		ContentResolver resolver = context.getContentResolver();
		Uri uri = VisitsTableMetaData.CONTENT_URI;

        if (visit.getId() >= 0)
        {
        	if(resolver.update(uri, cv, AnticaffeeProviderMetaData.VisitsTableMetaData._ID + "=" + visit.getId(), null) > 0)
        	{
        		resolver.notifyChange(VisitsTableMetaData.CONTENT_URI, null);
        		return visit;
        	}
        }
        else
        {
        	Uri inserted = resolver.insert(uri, cv);
        	 resolver.notifyChange(VisitsTableMetaData.CONTENT_URI, null);
        	long id = Long.parseLong(inserted.getPathSegments().get(1));
	        if (id < 0)
	        {
	        	return null;
	        }
	        return visit.copyWithID(id);
        }
		return null;
	}
	
	public Visit getLastVisit(int cardNumber)
	{
		return getVisit(AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_CARD_NUMBER + "=" + cardNumber);
	}
	
	public Visit getVisitWithID(long visitID)
	{
		return getVisit(AnticaffeeProviderMetaData.VisitsTableMetaData._ID + "=" + visitID);
	}
	
	private Visit getVisit(String condition)
	{
		ContentResolver resolver = context.getContentResolver();
		Uri uri = VisitsTableMetaData.CONTENT_URI;
		Cursor cursor = resolver.query(uri, AnticaffeeProviderMetaData.columns, condition, null, AnticaffeeProviderMetaData.VisitsTableMetaData.DEAFULT_SORT_ORDER);
		Visit result = null;
		cursor.moveToFirst();
		if (cursor.getCount() > 0)
		{
			result = readVisitFromCursor(cursor);
		}
		cursor.close();		
		return result;
	}
	
	public static Visit readVisitFromCursor(Cursor cursor)
	{
		Date enterDate = new Date(cursor.getLong(3));
		Date exitDate = cursor.isNull(4)?null:new Date(cursor.getLong(4));
		Visit visit = new Visit(cursor.getLong(0), cursor.getInt(1), cursor.getInt(2), enterDate, exitDate);
		if (!cursor.isNull(5))
		{
			visit.setCostPerPerson(cursor.getInt(5));
		}
		return visit;
	}
}

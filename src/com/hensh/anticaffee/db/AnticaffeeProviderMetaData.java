package com.hensh.anticaffee.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class AnticaffeeProviderMetaData
{
	public static final String AUTHORITY = "com.hensh.anticaffee.db.AnticaffeeProvider";
	
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "anticaffee";
	public static final String TABLE_NAME_VISIT = "visits";
	
	private AnticaffeeProviderMetaData(){}
	
	public static final class VisitsTableMetaData implements BaseColumns
	{
		private VisitsTableMetaData(){}
		public static final String TABLE_NAME = TABLE_NAME_VISIT;
		
		//����� �����
		public static final String VISIT_CARD_NUMBER = "card_number";
		
		//����� �����
		public static final String VISIT_PERSON_COUNT = "person_count";
		
		//����� �����, ������������
		public static final String VISIT_ENTER_DATE = "enter_date";
		
		//����� �����, ������������
		public static final String VISIT_EXIT_DATE = "exit_date";
		
		//����� �����
		public static final String VISIT_COST_PER_PERSON = "cost_per_person";
		
		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/visits");
		public static final String VISIT_BY_ID = "with_id";
		public static final String VISIT_BY_CARD_NUMBER = "with_card_number";
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.anticaffee.visit";
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.anticaffee.visit";
		
		public static final String DEAFULT_SORT_ORDER = "case when " + VISIT_EXIT_DATE + " is null then 0 else 1 end, " + VISIT_EXIT_DATE + " desc, " + VISIT_ENTER_DATE + " desc";
	}
	
	public static final String[] columns = 
		{
			AnticaffeeProviderMetaData.VisitsTableMetaData._ID, 
			AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_CARD_NUMBER, 
			AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_PERSON_COUNT,
			AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_ENTER_DATE, 
			AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE,
			AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_COST_PER_PERSON
		};
}

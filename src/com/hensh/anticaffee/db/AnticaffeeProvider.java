package com.hensh.anticaffee.db;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.hensh.anticaffee.db.AnticaffeeProviderMetaData.VisitsTableMetaData;

public class AnticaffeeProvider extends ContentProvider
{
	private static final String TAG = "AnticaffeeProvider";
	
	private static Map<String, String> visitsProjectionMap;
	static
	{
		visitsProjectionMap = new HashMap<String, String>();
		
		visitsProjectionMap.put(VisitsTableMetaData._ID, VisitsTableMetaData._ID);
		visitsProjectionMap.put(VisitsTableMetaData.VISIT_CARD_NUMBER, VisitsTableMetaData.VISIT_CARD_NUMBER);
		visitsProjectionMap.put(VisitsTableMetaData.VISIT_COST_PER_PERSON, VisitsTableMetaData.VISIT_COST_PER_PERSON);
		visitsProjectionMap.put(VisitsTableMetaData.VISIT_ENTER_DATE, VisitsTableMetaData.VISIT_ENTER_DATE);
		visitsProjectionMap.put(VisitsTableMetaData.VISIT_EXIT_DATE, VisitsTableMetaData.VISIT_EXIT_DATE);
		visitsProjectionMap.put(VisitsTableMetaData.VISIT_PERSON_COUNT, VisitsTableMetaData.VISIT_PERSON_COUNT);
	}
	
	private static UriMatcher uriMatcher;
	private static final int INCOMING_SINGLE_VISIT_URI_INDICATOR = 1;
	private static final int INCOMING_VISITS_COLLECTION_URI_INDICATOR = 2;
	private static final int INCOMING_VISITS_COLLECTION_BY_CARD_NUMBER_URI_INDICATOR = 3;
	
	static
	{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		
		uriMatcher.addURI(AnticaffeeProviderMetaData.AUTHORITY, "visits/with_id/#", INCOMING_SINGLE_VISIT_URI_INDICATOR);
		uriMatcher.addURI(AnticaffeeProviderMetaData.AUTHORITY, "visits", INCOMING_VISITS_COLLECTION_URI_INDICATOR);
		uriMatcher.addURI(AnticaffeeProviderMetaData.AUTHORITY, "visits/with_card_number", INCOMING_VISITS_COLLECTION_BY_CARD_NUMBER_URI_INDICATOR);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		private static final String CREATE_TABLE = "create table " + AnticaffeeProviderMetaData.TABLE_NAME_VISIT + " ( " + 
				 AnticaffeeProviderMetaData.VisitsTableMetaData._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				 AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_CARD_NUMBER + " INTEGER NOT NULL, " +
				 AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_PERSON_COUNT + " INTEGER NOT NULL, " +
				 AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_ENTER_DATE + " INTEGER NOT NULL, " +
				 AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_EXIT_DATE + " INTEGER, " +
				 AnticaffeeProviderMetaData.VisitsTableMetaData.VISIT_COST_PER_PERSON + " INTEGER DEFAULT NULL)";

		  public DatabaseHelper(Context context) 
		  {
			  super(context, AnticaffeeProviderMetaData.DB_NAME, null, AnticaffeeProviderMetaData.DB_VERSION);
		  }

		  @Override
		  public void onCreate(SQLiteDatabase sqLiteDatabase) 
		  {
			  Log.d(TAG, "inner on create called");
			  sqLiteDatabase.execSQL(CREATE_TABLE);
		  }

		  @Override
		  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) 
		  {
			  Log.d(TAG, "inner onupgrade called");
			  Log.w(TAG, "Upgrade database from " + oldVersion + " old version to " + newVersion + " new version. All data will deleted");
			  sqLiteDatabase.execSQL("DROP TABLE IS EXISTS " + VisitsTableMetaData.TABLE_NAME);
			  onCreate(sqLiteDatabase);
		  }
	}
	
	private DatabaseHelper openHelper;
	
	@Override
	public boolean onCreate()
	{
		Log.d(TAG, "main onCreate called");
		openHelper = new DatabaseHelper(getContext());
		return true;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch (uriMatcher.match(uri)) 
		{
			case INCOMING_VISITS_COLLECTION_URI_INDICATOR:
				qb.setTables(VisitsTableMetaData.TABLE_NAME);
				qb.setProjectionMap(visitsProjectionMap);
				break;
				
			case INCOMING_SINGLE_VISIT_URI_INDICATOR:
				qb.setTables(VisitsTableMetaData.TABLE_NAME);
				qb.setProjectionMap(visitsProjectionMap);
				qb.appendWhere(VisitsTableMetaData._ID + "=" + uri.getPathSegments().get(1));
				break;
				
			case INCOMING_VISITS_COLLECTION_BY_CARD_NUMBER_URI_INDICATOR:
				qb.setTables(VisitsTableMetaData.TABLE_NAME);
				qb.setProjectionMap(visitsProjectionMap);
				qb.appendWhere(VisitsTableMetaData.VISIT_CARD_NUMBER + "=" + uri.getPathSegments().get(1));
				break;
	
			default:
				throw new IllegalArgumentException("Unlnown URI " + uri);
		}
		
		String sortedBy = VisitsTableMetaData.DEAFULT_SORT_ORDER;
		if (!TextUtils.isEmpty(sortOrder))
		{
			sortedBy = sortOrder;
		}
		SQLiteDatabase db = openHelper.getReadableDatabase();
		Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortedBy);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}
	
	@Override
	public String getType(Uri uri)
	{
		switch (uriMatcher.match(uri))
		{
			case INCOMING_SINGLE_VISIT_URI_INDICATOR:
				return VisitsTableMetaData.CONTENT_ITEM_TYPE;
				
			case INCOMING_VISITS_COLLECTION_URI_INDICATOR:
				return VisitsTableMetaData.CONTENT_TYPE;
				
			case INCOMING_VISITS_COLLECTION_BY_CARD_NUMBER_URI_INDICATOR:
				return VisitsTableMetaData.CONTENT_TYPE;
	
			default:
				throw new IllegalArgumentException("unknown URI " + uri);
		}
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues initualValues)
	{
		if (uriMatcher.match(uri) != INCOMING_VISITS_COLLECTION_URI_INDICATOR)
		{
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		
		ContentValues values;
		if (initualValues != null)
		{
			values = new ContentValues(initualValues);
		}
		else
		{
			values = new ContentValues();
		}
		if (values.containsKey(VisitsTableMetaData.VISIT_CARD_NUMBER) == false)
		{
			throw new SQLiteException("Failed to insert row because Visits cars number is needed");
		}
		if (values.containsKey(VisitsTableMetaData.VISIT_PERSON_COUNT) == false)
		{
			throw new SQLiteException("Failed to insert row because Visits persons count is needed");
		}
		if (values.containsKey(VisitsTableMetaData.VISIT_ENTER_DATE) == false)
		{
			throw new SQLiteException("Failed to insert row because Visits enter date is needed");
		}
		
		SQLiteDatabase db = openHelper.getWritableDatabase();
		long rowId = db.insert(VisitsTableMetaData.TABLE_NAME, null, values);
		if (rowId < 0)
		{
			throw new SQLiteException("Failed to insert row into " + uri);
		}
		Uri insertedUri = ContentUris.withAppendedId(VisitsTableMetaData.CONTENT_URI, rowId);
		getContext().getContentResolver().notifyChange(insertedUri, null);
		
		return insertedUri;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		SQLiteDatabase db = openHelper.getWritableDatabase();
		int count;
		switch (uriMatcher.match(uri)) 
		{
			case INCOMING_VISITS_COLLECTION_URI_INDICATOR:
				count = db.update(VisitsTableMetaData.TABLE_NAME, values, selection, selectionArgs);				
				break;
				
			case INCOMING_SINGLE_VISIT_URI_INDICATOR:
				String rowId = uri.getPathSegments().get(1);
				count = db.update(
						VisitsTableMetaData.TABLE_NAME, 
						values, 
						VisitsTableMetaData._ID + "=" + rowId + (!TextUtils.isEmpty(selection)?" AND (" + selection + ")":""),
						selectionArgs);
						
				break;
	
			default:
				throw new  IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs)
	{
		SQLiteDatabase db = openHelper.getWritableDatabase();
		int count;
		switch (uriMatcher.match(uri))
		{
			case INCOMING_VISITS_COLLECTION_URI_INDICATOR:
				count = db.delete(VisitsTableMetaData.TABLE_NAME, selection, selectionArgs);
				break;
				
			case INCOMING_SINGLE_VISIT_URI_INDICATOR:
				String rowId = uri.getPathSegments().get(1);
				count = db.delete(
						VisitsTableMetaData.TABLE_NAME, 
						VisitsTableMetaData._ID + "=" + rowId + (!TextUtils.isEmpty(selection)?" AND (" + selection + ")":""), selectionArgs);
				break;
	
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
}

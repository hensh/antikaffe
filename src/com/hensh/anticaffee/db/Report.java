package com.hensh.anticaffee.db;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.CellView;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.UnderlineStyle;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import android.content.Context;

import com.hensh.anticaffee.R;
import com.hensh.anticaffee.model.Visit;

public abstract class Report
{
	static final int MinColumnIndex = 0;
	static final int ColumnCardNumber = 0;
	static final int ColumnPersonsCount = 1;
	static final int ColumnEnterDate = 2;
	static final int ColumnExitDate = 3;
	static final int ColumnCostPerPerson = 4;
	static final int ColumnTotalCost = 5;
	static final int MaxColumnIndex = 5;
	
	static final int TotalSummRowOffset = 1;
	static final int TopRowContentOffset = 3;
	static final int RowHeader = 2;
	static final int RowTitle = 0;
	
	static final int HeaderTitleOffsetInChars = 6;
	
	static WritableCellFormat timesBoldUnderline = null;
	static WritableCellFormat times = null;
	
	static WritableCellFormat getTimes() throws WriteException
	{
		if (times == null)
		{
			WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
			times = new WritableCellFormat(times10pt);
			times.setWrap(true);
		}
		return times;
	}
	
	static WritableCellFormat getTimesBoldUnderline() throws WriteException
	{
		if (timesBoldUnderline == null)
		{
			WritableFont times10ptBoldUnderline = new WritableFont(
					WritableFont.TIMES, 10, WritableFont.BOLD, false,
					UnderlineStyle.SINGLE);
			timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
			timesBoldUnderline.setWrap(true);
		}
		return timesBoldUnderline;
	}

	public static boolean export(File file, Context context, VisitsDataSource dataSource)
	{
		try
		{					
			WritableWorkbook workBook = Workbook.createWorkbook(file);
		
			workBook.createSheet(context.getString(R.string.report), 0);
			WritableSheet excelSheet = workBook.getSheet(0);
			
			writeTitle(excelSheet, dataSource.getStartDate(), dataSource.getFinishDate(), context);
			writeHeader(excelSheet, context);
		    writeContent(excelSheet, dataSource, context);
	
		    workBook.write();
		    workBook.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		} 
		catch (WriteException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private static void writeTitle(WritableSheet sheet, Date startDate, Date finishDate, Context context) throws RowsExceededException, WriteException
	{
		String reportTitle = getReportName(startDate, finishDate, context);
		addLabelInAllWidth(sheet, RowTitle, reportTitle, Alignment.CENTRE);
	}

	private static void writeHeader(WritableSheet sheet, Context context) throws WriteException
	{
		CellView cv = new CellView();
		cv.setFormat(getTimes());
		cv.setFormat(getTimesBoldUnderline());
		cv.setAutosize(true);

		String cardNumberTitle = context.getString(R.string.card_number);
		addLabel(sheet, ColumnCardNumber, RowHeader, cardNumberTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnCardNumber, cardNumberTitle.length() + HeaderTitleOffsetInChars);
		
		String personsCountTitle = context.getString(R.string.persons_count);
		addLabel(sheet, ColumnPersonsCount, RowHeader, personsCountTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnPersonsCount, personsCountTitle.length() + HeaderTitleOffsetInChars);
		
		String enterDateTitle = context.getString(R.string.enter_time);
		addLabel(sheet, ColumnEnterDate, RowHeader, enterDateTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnEnterDate, enterDateTitle.length() + HeaderTitleOffsetInChars);
		
		String exitDateTitle = context.getString(R.string.enter_time);
		addLabel(sheet, ColumnExitDate, RowHeader, exitDateTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnExitDate, exitDateTitle.length() + HeaderTitleOffsetInChars);
		
		String costPerPersonTitle = context.getString(R.string.cost_per_person);
		addLabel(sheet, ColumnCostPerPerson, RowHeader, costPerPersonTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnCostPerPerson, costPerPersonTitle.length() + HeaderTitleOffsetInChars);
		
		String totalCostTitle = context.getString(R.string.total_cost);
		addLabel(sheet, ColumnTotalCost, RowHeader, totalCostTitle, Alignment.CENTRE);
		sheet.setColumnView(ColumnTotalCost, totalCostTitle.length() + HeaderTitleOffsetInChars);
	}

	private static void writeVisit(WritableSheet sheet, Visit visit, DataStorage dataSource, int position) throws RowsExceededException, WriteException
	{
		DateFormat timeFormat = SimpleDateFormat.getDateTimeInstance();
		addNumber(sheet, ColumnCardNumber, position, visit.getCardNumber(), Alignment.CENTRE);
		addNumber(sheet, ColumnPersonsCount, position, visit.getPersonsCount(), Alignment.CENTRE);
		addLabel(sheet, ColumnEnterDate, position, timeFormat.format(visit.getEnterDate()), Alignment.CENTRE);
		addLabel(sheet, ColumnExitDate, position, timeFormat.format(visit.getExitDate()), Alignment.CENTRE);
		
		if (visit.isActive())
		{
			return;
		}
		
		int costPerPerson = visit.getCostPerPerson();
		addNumber(sheet, ColumnCostPerPerson, position, costPerPerson, Alignment.RIGHT);
		addNumber(sheet, ColumnTotalCost, position, costPerPerson * visit.getPersonsCount(), Alignment.RIGHT);
	}
	
	private static char getColomnChar(int colomn)
	{
		return (char) ('A' + colomn);
	}
	
	private static boolean writeContent(WritableSheet sheet, VisitsDataSource dataSource, Context context) throws WriteException,
			RowsExceededException
	{
		DataStorage dataStorage = new DataStorage(context);
		int rowCount = dataSource.getCount();
		for (int i = 0; i < rowCount; i++)
		{
			Visit visit = dataSource.getVisit(i);
			if (!visit.isActive())
			{
				writeVisit(sheet, visit, dataStorage, i + TopRowContentOffset);
			}
		}
		
		char rowTotalCostChar = getColomnChar(ColumnTotalCost);  
		int bottomRow = rowCount + TotalSummRowOffset + TopRowContentOffset;
		String formulaString = "SUM(" + rowTotalCostChar + TopRowContentOffset + ":" + rowTotalCostChar + bottomRow + ")";
		Formula f = new Formula(ColumnTotalCost, bottomRow,  formulaString);
		sheet.addCell(f);
		
		addLabel(sheet, ColumnTotalCost-1, bottomRow, context.getString(R.string.total_summ), Alignment.RIGHT);
		
		return true;
	}

	private static void addCaption(WritableSheet sheet, int column, int row, String s, Alignment alignment)
			throws RowsExceededException, WriteException
	{
		Label label = new Label(column, row, s, getTimesBoldUnderline());
		setAligment(label, alignment);
		sheet.addCell(label);
	}
	
	@SuppressWarnings("unused")
	//TODO ����������� ������ ���� ����������� � UTC
	private static void addDate(WritableSheet sheet, int colomn, int row, Date date, Alignment alignment) throws RowsExceededException, WriteException
	{
		if (date != null)
		{
			DateTime dateTime = new DateTime(colomn, row, date);
			setAligment(dateTime, alignment);
			sheet.addCell(dateTime);
		}
	}
	
	private static void addNumber(WritableSheet sheet, int column, int row,
			long integer, Alignment alignment) throws WriteException, RowsExceededException
	{
		Number number = new Number(column, row, integer, getTimes());;
		setAligment(number, alignment);
		sheet.addCell(number);
	}

	private static void addLabel(WritableSheet sheet, int column, int row, String s, Alignment alignment)
			throws WriteException, RowsExceededException
	{
		Label label = new Label(column, row, s, getTimes()); 
		setAligment(label, alignment);
		sheet.addCell(label);
	}
	
	private static void setAligment(WritableCell cell, Alignment alignment) throws WriteException
	{
		WritableCellFormat format = new WritableCellFormat();
		format.setAlignment(alignment);
		cell.setCellFormat(format);
	}
	
	private static void addLabelInAllWidth(WritableSheet sheet, int row, String string, Alignment alignment) throws RowsExceededException, WriteException
	{
		sheet.mergeCells(MinColumnIndex, row, MaxColumnIndex, row);
		addCaption(sheet, MinColumnIndex, row, string, alignment);
	}
	
	public static String getReportName(Date startDate, Date finishDate, Context context)
	{
		DateFormat timeFormat = SimpleDateFormat.getDateInstance();
		String reportTitle = context.getString(R.string.report);	
		if (startDate != null)
		{
			reportTitle += " " + context.getString(R.string.start_date_label) + " " + timeFormat.format(startDate);
		}
		if (finishDate != null)
		{
			reportTitle += " " + context.getString(R.string.end_date_label) + " " + timeFormat.format(finishDate);
		}
		return reportTitle;
	}
}

package com.hensh.anticaffee;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.UI.VisitsAdapter;
import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.db.VisitsDataSource;
import com.hensh.anticaffee.model.Visit;

public class VisitsTableActivity extends Activity
{
	private static final String BLOCK_FLAG = "block flag"; 
	
	static final int ShowActiveOnlyID = 0;
	static final int CreateReportID = 1;
	static final int CalculatorSettingsID = 2;

	static final String ShowActiveOnlyPref = "ShowActiveOnlyPref";

	private VisitsDataSource visitsDataSource;
	private int firstVisiblePosition = 0;
	private int topPosition = 0;
	private boolean showActiveOnly;
	private int displayWidth;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_calculator);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;
		
		setTextViewSizeWidth();
		setButtonTextSize();

		View header = findViewById(R.id.header);
		registerForContextMenu(header);

		ListView visitsListView = (ListView) findViewById(R.id.visitsListView);
		visitsListView.setOnItemLongClickListener(new OnItemLongClickListener()
		{

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
			{
				final Visit selectedFisit = visitsDataSource.getVisit(position);
				AlertDialog alertDialog = new AlertDialog.Builder(VisitsTableActivity.this).create();
				java.text.DateFormat format = DateFormat.getTimeFormat(VisitsTableActivity.this);
				alertDialog.setTitle(getString(R.string.are_you_shore_delete_this_visit_));
				alertDialog.setMessage(getString(R.string.card_number) + " " + selectedFisit.getCardNumber() + "\n" +
								getString(R.string.enter_time) + format.format(selectedFisit.getEnterDate()));
				alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						DataStorage dataStore = new DataStorage(VisitsTableActivity.this);
						dataStore.delete(selectedFisit );
						refreshTable();
					}
				});
				alertDialog.setButton2(getString(R.string.cancel), (Message)null);

				alertDialog.show();
				return false;
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		showActiveOnly = preferences.getBoolean(ShowActiveOnlyPref, true);

		refreshTable();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (prefs.getBoolean(BLOCK_FLAG, false))
		{
			finish();
		}
		else
		{
			Date now = new Date();
			if (now.getYear() + 1900  >= 2014 && now.getMonth() >= Calendar.MARCH && now.getDate() >= 5)
			{
				prefs.edit().putBoolean(BLOCK_FLAG, true).commit();
				finish();
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(getString(R.string.menu));

		menu.add(Menu.NONE, ShowActiveOnlyID, Menu.NONE, getShowModeTitleResId());
		menu.add(Menu.NONE, CreateReportID, Menu.NONE, R.string.create_report);
		menu.add(Menu.NONE, CalculatorSettingsID, Menu.NONE, R.string.calculator_preferences);
//		menu.add(Menu.NONE, CreateDebugInfo, Menu.NONE, R.string.create_debug_info);
	}

	private int getShowModeTitleResId()
	{
		return showActiveOnly ? R.string.show_all_visits : R.string.show_only_active_visits;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
		case ShowActiveOnlyID:
			showActiveOnly = !showActiveOnly;
			refreshTable();
			item.setTitle(getShowModeTitleResId());
			break;

		case CreateReportID:
			Intent reportIntent = new Intent(this, ReportActivity.class);
			startActivity(reportIntent);
			break;

		case CalculatorSettingsID:
			Intent calculatorIntent = new Intent(this, CalculatorPreferencesActivity.class);
			startActivity(calculatorIntent);
			break;

		default:
			return false;
		}
		return true;
	}

	void setVisitsDataSource(VisitsDataSource newVisitsDataSource)
	{
		if (visitsDataSource == newVisitsDataSource)
		{
			return;
		}
		if (visitsDataSource != null)
		{
			visitsDataSource.close();
		}
		visitsDataSource = newVisitsDataSource;
	}

	private void refreshTable()
	{
		ListView visitsListView = (ListView) findViewById(R.id.visitsListView);
		int mode;
		TextView extTitleTextView = (TextView) findViewById(R.id.mainCalcExitTimeTextView);
		if (showActiveOnly)
		{
			mode = VisitsDataSource.ActiveOnly;
			extTitleTextView.setVisibility(View.GONE);			
		}
		else
		{
			mode = VisitsDataSource.AllVisits;
			extTitleTextView.setVisibility(View.VISIBLE);
		}
		setVisitsDataSource(new VisitsDataSource(mode, this.getContentResolver()));
		VisitsAdapter visitsAdapter = new VisitsAdapter(this, visitsDataSource, !showActiveOnly);
		if (visitsDataSource.getCount() > 0)
		{
			visitsListView.setAdapter(visitsAdapter);
			visitsListView.setOnItemClickListener(new OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> adapter, View view, int position, long id)
				{
					Intent intent = new Intent(VisitsTableActivity.this, CalculateBillActivity.class);
					long visitID = visitsDataSource.getVisitID(position);
					intent.putExtra(UIConsts.ExtrasVisitID, visitID);
					startActivity(intent);
				}
			});
			visitsListView.setSelectionFromTop(firstVisiblePosition, topPosition);
		} else
		{
			visitsListView.setAdapter(null);
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		ListView visitsListView = (ListView) findViewById(R.id.visitsListView);
		firstVisiblePosition = visitsListView.getFirstVisiblePosition();
		View v = visitsListView.getChildAt(0);
		topPosition = (v == null) ? 0 : v.getTop();

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.edit().putBoolean(ShowActiveOnlyPref, showActiveOnly).commit();
	}

	public void createNewVisitAction(View view)
	{
		Intent createNewVisitIntent = new Intent(this, CreateNewVisitActivity.class);
		startActivity(createNewVisitIntent);
	}

	public void searchVisitAction(View view)
	{
		Intent finishVisitIntent = new Intent(this, SearchVisitActivity.class);
		startActivity(finishVisitIntent);
	}
	
	private int textViewSizeWidth()
	{
		return displayWidth / 36;
	}
	
	private int buttonTextSize()
	{
		return displayWidth / 26;
	}
	
	private void setTextViewSizeWidth()
	{
		TextView mainCalcCardNumberTextView = (TextView) findViewById(R.id.mainCalcCardNumberTextView);
		TextView mainCalcPersonsCountTextView = (TextView) findViewById(R.id.mainCalcPersonsCountTextView);
		TextView mainCalcEnterTimeTextView = (TextView) findViewById(R.id.mainCalcEnterTimeTextView);
		TextView mainCalcExitTimeTextView = (TextView) findViewById(R.id.mainCalcExitTimeTextView);
		
		mainCalcCardNumberTextView.setTextSize(textViewSizeWidth());
		mainCalcPersonsCountTextView.setTextSize(textViewSizeWidth());
		mainCalcEnterTimeTextView.setTextSize(textViewSizeWidth());
		mainCalcExitTimeTextView.setTextSize(textViewSizeWidth());
	}
	
	private void setButtonTextSize()
	{
		Button newVisitButton = (Button) findViewById(R.id.newVisitButton);
		Button searchVisitButton = (Button) findViewById(R.id.searchVisitButton);
		
		newVisitButton.setTextSize(buttonTextSize());
		searchVisitButton.setTextSize(buttonTextSize());
	}
}


package com.hensh.anticaffee;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hensh.anticaffee.UI.CountSelectorDialogFragment;
import com.hensh.anticaffee.UI.CountSelectorDialogFragment.Listener;
import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.model.IPurchaseCalculator;
import com.hensh.anticaffee.model.Visit;

public class CalculateBillActivity extends FragmentActivity
{
	private static final int MIN_SPEND_MINUTS = 30;
	
	private DataStorage dataStorage;
	private java.text.DateFormat dateFormat;
	private String format = "HH:mm";
	private int displayWidth;
	private Visit visit;
	private AppPrefs prefs = null; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		prefs = new AppPrefs(this);
		setContentView(R.layout.activity_calculate_bill);
		dataStorage = new DataStorage(this);
		dateFormat = new SimpleDateFormat(format, Locale.getDefault());
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;
		
		setTextViewSizeWidth();
		setButtonSize();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		dataStorage = null;
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		Intent intent = getIntent();
		long visitID = intent.getLongExtra(UIConsts.ExtrasVisitID, Visit.UndefinedID);
		visit = dataStorage.getVisitWithID(visitID);
		
		if (visit == null)
		{
			return;
		}
		
		updateUI();
	}
	
	private int getBillForVisit(Visit visit)
	{
		IPurchaseCalculator calculator = prefs.getCalculator();
		
		Date exitTime = visit.getExitDate();
		if (exitTime == null)
		{
			exitTime = new Date();
		}
		Date enterTime = visit.getEnterDate();
		if (TimeUnit.MILLISECONDS.toMinutes(exitTime.getTime() - enterTime.getTime()) < MIN_SPEND_MINUTS)
		{
			exitTime = new Date(enterTime.getTime() + TimeUnit.MINUTES.toMillis(MIN_SPEND_MINUTS));
		}
		return calculator.calculateCost(enterTime, exitTime);
	}
	
	public void finishAction(View view)
	{
		visit.finish();
		visit = dataStorage.save(visit); 
		int costPerPerson = getBillForVisit(visit);
		visit.setCostPerPerson(costPerPerson);
		dataStorage.save(visit);
		
		updateUI(); 
	}
	
	public void reopenAction(View view)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		java.text.DateFormat format = DateFormat.getTimeFormat(this);
		alertDialog.setTitle(getString(R.string.are_you_shore_to_reopen_visit));
		alertDialog.setMessage(getString(R.string.card_number) + " " + visit.getCardNumber() + "\n" +
						getString(R.string.enter_time) + format.format(visit.getEnterDate()));
		alertDialog.setButton(getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				visit.reopen();
				visit = dataStorage.save(visit);
				finish();
			}
		});
		alertDialog.setButton2(getString(R.string.cancel), (Message)null);

		alertDialog.show();
	}
	
	public void separateAction(View view)
	{
		if (visit.getPersonsCount() < 2)
		{
			return;
		}
			
		CountSelectorDialogFragment countSelectorDialog = new CountSelectorDialogFragment();
		countSelectorDialog.setVisit(visit);
		countSelectorDialog.setListener(new Listener()
		{
			@Override
			public void didSeparate()
			{
				finish();
			}
		});
		countSelectorDialog.show(getSupportFragmentManager(), "CountSelectorDialogFragment");
	}
	
	private void updateUI()
	{
		
		TextView cardNumberTextView = (TextView) findViewById(R.id.cardNumberTextView);
		cardNumberTextView.setText(String.valueOf(visit.getCardNumber()));
		
		TextView personsCountTextView = (TextView) findViewById(R.id.personsCountTextView);
		personsCountTextView.setText(String.valueOf(visit.getPersonsCount()));
		
		TextView enterDateTextView = (TextView) findViewById(R.id.enterDateTextView);
		enterDateTextView.setText(dateFormat.format(visit.getEnterDate()));
		
		TextView finishDateTextView = (TextView) findViewById(R.id.exitDateTextView);
		String exitDateString = "";
		if (visit.getExitDate() != null)
		{
			exitDateString = dateFormat.format(visit.getExitDate());
		}
		finishDateTextView.setText(exitDateString);
		
		int costPerPerson = getBillForVisit(visit);
		
		TextView costPerPeronTextView = (TextView) findViewById(R.id.costPerPersonTextView);
		costPerPeronTextView.setText(String.valueOf(costPerPerson) + " " + getString(R.string.curency_short));
		
		TextView totalCostTextView = (TextView) findViewById(R.id.totalCostTextView);
		totalCostTextView.setText(String.valueOf(costPerPerson*visit.getPersonsCount()) + " " + getString(R.string.curency_short));
		
		int visibility = visit.isActive()?View.VISIBLE:View.GONE;
		
		Button finishButton = (Button) findViewById(R.id.finishVisitButton);
		if (visit.getPersonsCount() <= 1)
		{
			finishButton.setText(R.string.finish_visit);
		}
		else
		{
			finishButton.setText(R.string.finish_visit_for_all);
		}
		
		View finishTableRow = findViewById(R.id.finishVisitTableRow);
		finishTableRow.setVisibility(visibility);
		
		if (visit.getPersonsCount() <= 1)
		{
			visibility = View.GONE;
		}
		View separateTableRow = findViewById(R.id.separateVisitTableRow);
		separateTableRow.setVisibility(visibility);
		
		View reopenTableRow = findViewById(R.id.reopenVisitTableRow);
		reopenTableRow.setVisibility(visit.isActive()?View.GONE:View.VISIBLE);
	}
	
	private int textViewSizeWidth()
	{
		return displayWidth / 27;
	}
	
	private void setTextViewSizeWidth()
	{
		TextView cardNumberTitleTextView = (TextView) findViewById(R.id.cardNumberTitleTextView);
		TextView cardNumberTextView = (TextView) findViewById(R.id.cardNumberTextView);
		TextView personsCountTitleTextView = (TextView) findViewById(R.id.personsCountTitleTextView);
		TextView personsCountTextView = (TextView) findViewById(R.id.personsCountTextView);
		TextView enterDateTitleTextView = (TextView) findViewById(R.id.enterDateTitleTextView);
		TextView enterDateTextView = (TextView) findViewById(R.id.enterDateTextView);
		TextView exitDateTitleTextView = (TextView) findViewById(R.id.exitDateTitleTextView);
		TextView exitDateTextView = (TextView) findViewById(R.id.exitDateTextView);
		TextView costPerPersonTitleTextView = (TextView) findViewById(R.id.costPerPersonTitleTextView);
		TextView costPerPersonTextView = (TextView) findViewById(R.id.costPerPersonTextView);
		TextView totalCostTitleTextView = (TextView) findViewById(R.id.totalCostTitleTextView);
		TextView totalCostTextView = (TextView) findViewById(R.id.totalCostTextView);
		
		cardNumberTitleTextView.setTextSize(textViewSizeWidth());
		personsCountTitleTextView.setTextSize(textViewSizeWidth());
		cardNumberTextView.setTextSize(textViewSizeWidth());
		personsCountTextView.setTextSize(textViewSizeWidth());
		enterDateTitleTextView.setTextSize(textViewSizeWidth());
		enterDateTextView.setTextSize(textViewSizeWidth());
		exitDateTitleTextView.setTextSize(textViewSizeWidth());
		exitDateTextView.setTextSize(textViewSizeWidth());
		costPerPersonTitleTextView.setTextSize(textViewSizeWidth());
		costPerPersonTextView.setTextSize(textViewSizeWidth());
		totalCostTitleTextView.setTextSize(textViewSizeWidth());
		totalCostTextView.setTextSize(textViewSizeWidth());
	}
	
	private void setButtonSize()
	{
		Button finishButton = (Button) findViewById(R.id.finishVisitButton);
		Button separateButton = (Button) findViewById(R.id.separate_button);
		
		finishButton.setTextSize(textViewSizeWidth());
		separateButton.setTextSize(textViewSizeWidth());
		
		Button reopenButton = (Button) findViewById(R.id.reopen_button);
		reopenButton.setTextSize(textViewSizeWidth());
	}
}

package com.hensh.anticaffee;

class AppConsts
{
	public static final int FirstHourDefaultCost = 2;
	public static final int SecondHourDefaultCost = 1;
	public static final boolean DefaultCalculationRounding = true;
}

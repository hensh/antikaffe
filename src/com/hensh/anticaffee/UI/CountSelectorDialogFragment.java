package com.hensh.anticaffee.UI;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.hensh.anticaffee.CalculateBillActivity;
import com.hensh.anticaffee.R;
import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.model.Visit;

public class CountSelectorDialogFragment extends DialogFragment implements OnClickListener
{
	public interface Listener
	{
		void didSeparate();
	}
	
	private Visit visit;
	private Listener listener;
	private EditText editText;
	
	public void setListener(Listener listener)
	{
		this.listener = listener;
	}
	
	public void setVisit(Visit visit)
	{
		this.visit = visit;
	}	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.count_selector_view, container);
		Dialog dialog = getDialog();
		dialog.setTitle(R.string.separate_company_);
		
		editText = (EditText) view.findViewById(R.id.personsCountEditText);
		editText.setOnKeyListener(new OnKeyListener()
		{			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP)
				{					
					separate();
					return true;
				}
				return false;
			}
		});
		
		return view;
	}

	public void onClick(DialogInterface dialog, int which)
	{
		switch (which) 
		{
			case Dialog.BUTTON_POSITIVE:
				separate();
				break;
				
			case Dialog.BUTTON_NEGATIVE:
				dismiss();
				break;
		}
	}
	
	private boolean separate()
	{
		String inputValue = editText.getText().toString();
		int personsCount ;
		try
		{
			personsCount = Integer.parseInt(inputValue);
		}
		catch (Throwable throwable)
		{
			Toast.makeText(getActivity(), R.string.enter_number, Toast.LENGTH_SHORT).show();
			return false;
		}
		if (personsCount == visit.getPersonsCount())
		{
			dismiss();
			return false;
		}
		if (personsCount > visit.getPersonsCount())
		{
			Toast.makeText(getActivity(), R.string.to_little_persons_registered_on_the_card, Toast.LENGTH_SHORT).show();
			return false;
		}
		Visit separatedVisit = visit.separate(personsCount);
		DataStorage dataStorage = new DataStorage(getActivity());
		separatedVisit = dataStorage.save(separatedVisit);
		visit = dataStorage.save(visit);
		
		Intent intent = new Intent(getActivity(), CalculateBillActivity.class);
		intent.putExtra(UIConsts.ExtrasVisitID, separatedVisit.getId());
		startActivity(intent);
		
		if (listener != null)
		{
			listener.didSeparate();
		}
		
		return true;
	}
}

package com.hensh.anticaffee.UI;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hensh.anticaffee.R;
import com.hensh.anticaffee.db.VisitsDataSource;
import com.hensh.anticaffee.model.Visit;

public class VisitsAdapter extends BaseAdapter
{
	private LayoutInflater inflater;
	private VisitsDataSource dataSource;
	private DateFormat timeFormat;
	private boolean showExitTable;
	
	public VisitsAdapter(Activity activity, VisitsDataSource dataSource, boolean showExitTable)
	{
		this.inflater = activity.getLayoutInflater();
		this.dataSource = dataSource;
		this.showExitTable = showExitTable;
		timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
	}

	@Override
	public int getCount()
	{
		return dataSource.getCount();
	}

	@Override
	public Object getItem(int position)
	{
		return dataSource.getVisit(position);
	}

	@Override
	public long getItemId(int position)
	{
		Date enterDate = dataSource.getEnterDate(position);
		return TimeUnit.MINUTES.toDays(TimeUnit.MILLISECONDS.toMinutes(enterDate.getTime()) + enterDate.getTimezoneOffset());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = createItemView();
		}
		
		updateView(convertView, dataSource.getVisit(position));
		
		return convertView;
	}
	
	private View createItemView()
	{
		return inflater.inflate(R.layout.visit_item_view, null);
	}
	
	private void updateView(View view, Visit visit)
	{
		TextView cardNumberTextView = (TextView) view.findViewById(R.id.cardNumberTextView);
		cardNumberTextView.setText(String.valueOf(visit.getCardNumber()));
		
		TextView personsCountTextView = (TextView) view.findViewById(R.id.personsCountTextView);
		personsCountTextView.setText(String.valueOf(visit.getPersonsCount()));
		
		TextView enterDateTextView = (TextView) view.findViewById(R.id.enterDateTextView);
		enterDateTextView.setText(timeFormat.format(visit.getEnterDate()));
		
		TextView exitDateTextView = (TextView) view.findViewById(R.id.exitDateTextView);
		if (showExitTable)
		{
			Date exitDate = visit.getExitDate(); 
			String exitDateText = "";
			if (exitDate != null)
			{
				exitDateText = timeFormat.format(exitDate);
			}
			exitDateTextView.setText(exitDateText);
			exitDateTextView.setVisibility(View.VISIBLE);
		}
		else
		{
			exitDateTextView.setVisibility(View.GONE);
		}
	}
}

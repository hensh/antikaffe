package com.hensh.anticaffee.model;

import java.util.Date;

public interface IPurchaseCalculator
{
	int calculateCost(Date start, Date finish);
}

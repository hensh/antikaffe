package com.hensh.anticaffee.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SimplePurchaseCalculator implements Serializable, IPurchaseCalculator
{
	private static final long serialVersionUID = -7333226469371863020L;
	
	protected final int firstHourCost;
	protected final int nextHourCost;

	public int getFirstHourCost()
	{
		return firstHourCost;
	}

	public int getNextHourCost()
	{
		return nextHourCost;
	}

	public int calculateCost(Date start, Date finish)
	{
		int cost = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		long duration = Math.max(0, TimeUnit.MILLISECONDS.toMinutes(finish.getTime() - calendar.getTimeInMillis()));
		cost += Math.min(duration, 60) * 2;
		duration -= 60;
		if (duration > 0)
		{
			cost += duration;
		}
		return rounding(cost);
	}

	protected int rounding(int cost)
	{
		int roundingValue = cost % 10;
		cost -= cost % 10;
		if (roundingValue >= 5)
		{
			cost += 10;
		}
		return cost;
	}

	public SimplePurchaseCalculator(int firstHourCost, int nextHourCost)
	{
		this.firstHourCost = firstHourCost;
		this.nextHourCost = nextHourCost;
	}
}

package com.hensh.anticaffee.model;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PurchaseCalculator extends SimplePurchaseCalculator
{
	private static final long serialVersionUID = -2572958675809289808L;

	@Override
	public int calculateCost(Date start, Date finish)
	{
		int cost = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		long doubleMinuts = 0;
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY)
		{
			if (calendar.get(Calendar.HOUR_OF_DAY) < 17)
			{
				calendar.set(Calendar.HOUR_OF_DAY, 17);
				calendar.set(Calendar.MINUTE, 0);
				long finishMills = Math.min(calendar.getTimeInMillis(), finish.getTime());
				doubleMinuts = Math.max(0, TimeUnit.MILLISECONDS.toMinutes(finishMills - start.getTime()));
				cost += doubleMinuts;
			}
		}
		long duration = Math.max(0, TimeUnit.MILLISECONDS.toMinutes(finish.getTime() - calendar.getTimeInMillis()));
		long firstHour = Math.max(0, 60 - doubleMinuts);
		cost += Math.min(duration, firstHour) * 2;
		duration -= firstHour;
		if (duration > 0)
		{
			cost += duration;
		}
		return rounding(cost);
	}

	public PurchaseCalculator(int firstHourCost, int nextHourCost)
	{
		super(firstHourCost, nextHourCost);
	}
}

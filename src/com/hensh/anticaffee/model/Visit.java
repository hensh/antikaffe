package com.hensh.anticaffee.model;

import java.util.Date;

public class Visit
{
	public static final long UndefinedID = -1;
	
	private long id;
	private int cardNumber;
	private Date enterDate;
	private Date exitDate;
	private int personsCount;
	private int costPerPerson = 0;
	
	public long getId()
	{
		return id;
	}
	
	public int getCardNumber()
	{
		return cardNumber;
	}
	
	public Date getEnterDate()
	{
		return enterDate;
	}
	
	public Date getExitDate()
	{
		return exitDate;
	}
	
	public int getPersonsCount()
	{
		return personsCount;
	}
	
	public Visit(int cardNumber, int personsCount, Date enterDate)
	{
		this(-1, cardNumber, personsCount, enterDate);
	}
	
	public Visit(long id, int cardNumber, int personsCount, Date enterDate)
	{
		this(-1, cardNumber, personsCount, enterDate, null);
	}
	
	public Visit(long id, int cardNumber, int personsCount, Date enterDate, Date exitDate)
	{
		this.id = id;
		this.cardNumber = cardNumber;
		this.personsCount = personsCount;
		this.enterDate = enterDate;
		this.exitDate = exitDate;
	}
	
	public Visit copyWithID(long id)
	{
		return new Visit(id, cardNumber, personsCount, enterDate, exitDate);
	}
	
	public void reopen()
	{
		exitDate = null;
	}
	
	public void finish()
	{
		exitDate = new Date();
	}
	
	public boolean isActive()
	{
		return (exitDate == null);
	}
	
	public Visit separate(int separatedPersosCount)
	{
		if (separatedPersosCount >= personsCount)
		{
			return null;
		}
		Visit separatedVisit = this.copyWithID(UndefinedID);
		separatedVisit.personsCount = separatedPersosCount;
		this.personsCount -= separatedPersosCount;
		return separatedVisit;
	}
	
	public int getCostPerPerson()
	{
		return costPerPerson;
	}
	
	public void setCostPerPerson(int costPerPerson)
	{
		this.costPerPerson = costPerPerson;
	}
	
	public long getTotalCost()
	{
		return costPerPerson * personsCount;
	}
	
	//in milliseconds
	public long getDuration()
	{
		return ((exitDate!=null)?exitDate:new Date()).getTime() - enterDate.getTime();
	}
}

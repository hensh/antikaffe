package com.hensh.anticaffee.model;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

import android.content.Context;

public abstract class SerializationHelper
{
	public static Serializable load(String fileName, Context context)
	{
		Serializable result = null;
		try
		{
			InputStream inputStream = context.openFileInput(fileName);
			ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
			result = (Serializable) objInputStream.readObject();
			objInputStream.close();
			inputStream.close();
		} 
		catch (Throwable throwable)
		{
			throwable.printStackTrace();
		}
		
		return result;
	}
	
	public static boolean save(Serializable object, Context context, String fileName)
	{
		try
		{
			OutputStream outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			ObjectOutputStream objOutputStream = new ObjectOutputStream(outputStream );
			objOutputStream.writeObject(object);
			objOutputStream.close();
			outputStream.close();
		} 
		catch (Throwable trowable)
		{
			trowable.printStackTrace();
			return false;
		}	
		return true;
	}

}

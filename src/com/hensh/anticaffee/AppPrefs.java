package com.hensh.anticaffee;

import android.content.Context;
import android.content.SharedPreferences;

import com.hensh.anticaffee.model.IPurchaseCalculator;
import com.hensh.anticaffee.model.SimplePurchaseCalculator;

class AppPrefs
{
	private static final String PrefsName = "com.hensh.anticaffee.preferences";
	private static final String FirstHourCostKey = "FirstHourCostKey";
	private static final String SecondHourCostKey = "SecondHourCostKey";
	
	private SharedPreferences preferences;
	
	public AppPrefs(Context context)
	{
		preferences = context.getSharedPreferences(PrefsName, Context.MODE_PRIVATE);
	}

	public int getFirstHourCost()
	{
		return preferences.getInt(FirstHourCostKey, AppConsts.FirstHourDefaultCost);
	}
	
	public boolean setFirstHourCost(int newFirstHourCost)
	{
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(FirstHourCostKey, newFirstHourCost);
		return editor.commit();
	}
	
	public int getSecondHourCost()
	{
		return preferences.getInt(SecondHourCostKey, AppConsts.SecondHourDefaultCost);
	}
	
	public boolean setSecondHourCost(int newSecondHourCost)
	{
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SecondHourCostKey, newSecondHourCost);
		return editor.commit();
	}
	
	public boolean setCalculatorParams(int newFirstHourCost, int newSecondHourCost)
	{
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(FirstHourCostKey, newFirstHourCost);
		editor.putInt(SecondHourCostKey, newSecondHourCost);
		return editor.commit();
	}
	
	public IPurchaseCalculator getCalculator()
	{
		return new SimplePurchaseCalculator(getFirstHourCost(), getSecondHourCost());
//		return new PurchaseCalculator(getFirstHourCost(), getSecondHourCost());
	}
	
	public boolean reset()
	{
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		return editor.commit();
	}
}

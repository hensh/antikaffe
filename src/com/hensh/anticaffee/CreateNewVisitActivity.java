package com.hensh.anticaffee;

import java.util.Date;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.model.Visit;

public class CreateNewVisitActivity extends Activity
{
	private DataStorage storage;
	private int displayWidth;
	private int displayHeight;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_create_new_visit);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;
		displayHeight = metrics.heightPixels;

		setTextViewSizeWidth();
		setEditTextSizeWidth();

		storage = new DataStorage(this);

		final EditText personsCountEditText = (EditText) findViewById(R.id.personCountEditText);
		personsCountEditText.setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				personsCountEditText.setOnKeyListener(null);
				if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER)
				{
					try
					{
						Visit visit = createVisit();

						if (visit != null)
						{
							finish();
						}
						else
						{
							Toast.makeText(CreateNewVisitActivity.this, R.string.visit_creation_failed, Toast.LENGTH_SHORT).show();
							personsCountEditText.setOnKeyListener(this);
						}
					}
					catch (NumberFormatException ex)
					{
						Toast.makeText(CreateNewVisitActivity.this, R.string.illegal_nuber_format_exception, Toast.LENGTH_SHORT).show();
					}
					return true;
				}
				personsCountEditText.setOnKeyListener(this);
				return false;
			}
		});
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

		setTextViewSizeHeight();
		setEditTextSizeHeight();

		EditText cardNumberEditText = (EditText) findViewById(R.id.cardNumberEditText);
		String cardNumber = cardNumberEditText.getText().toString();

		EditText personCountEditText = (EditText) findViewById(R.id.personCountEditText);
		String personsCount = personCountEditText.getText().toString();

		setContentView(R.layout.activity_create_new_visit);

		setTextViewSizeWidth();
		setEditTextSizeWidth();

		cardNumberEditText.setText(cardNumber);
		personCountEditText.setText(personsCount);

	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		storage = null;
	}

	private Visit createVisit() throws NumberFormatException
	{
		int personsCount = getPersonsCount();
		int cardNumber = getCardNumber();

		Visit savedVisit = storage.save(new Visit(cardNumber, personsCount, new Date()));
		return savedVisit;
	}

	private int getCardNumber() throws NumberFormatException
	{
		EditText cardNumberEditText = (EditText) findViewById(R.id.cardNumberEditText);
		return Integer.parseInt(cardNumberEditText.getText().toString());
	}

	private int getPersonsCount() throws NumberFormatException
	{
		EditText personCountEditText = (EditText) findViewById(R.id.personCountEditText);
		return Integer.parseInt(personCountEditText.getText().toString());
	}

	//FIXME ������ ��� �������� ���!!!
	private int textViewSizeWidth()
	{
		return displayWidth / 26;
	}

	private int editTextSizeWidth()
	{
		return displayWidth / 16;
	}

	private int textViewSizeHeight()
	{
		return displayHeight / 16;
	}

	private double editTextSizeHeight()
	{
		return displayHeight / 9.6;
	}

	private int editTextMinWidth()
	{
		return displayWidth / 9;
	}

	private void setTextViewSizeHeight()
	{
		TextView cardNumberTextView = (TextView) findViewById(R.id.createVisitCardNumberTextView);
		TextView personsCountTextView = (TextView) findViewById(R.id.createVisitPersonsCountTextView);

		cardNumberTextView.setTextSize(textViewSizeHeight());
		personsCountTextView.setTextSize(textViewSizeHeight());
	}

	private void setEditTextSizeHeight()
	{
		EditText cardNumberEditText = (EditText) findViewById(R.id.cardNumberEditText);
		EditText personCountEditText = (EditText) findViewById(R.id.personCountEditText);

		cardNumberEditText.setTextSize((float) editTextSizeHeight());
		personCountEditText.setTextSize((float) editTextSizeHeight());

		cardNumberEditText.setMinimumWidth(editTextMinWidth());
		personCountEditText.setMinimumWidth(editTextMinWidth());
	}

	private void setTextViewSizeWidth()
	{
		TextView cardNumberTextView = (TextView) findViewById(R.id.createVisitCardNumberTextView);
		TextView personsCountTextView = (TextView) findViewById(R.id.createVisitPersonsCountTextView);

		cardNumberTextView.setTextSize(textViewSizeWidth());
		personsCountTextView.setTextSize(textViewSizeWidth());
	}

	private void setEditTextSizeWidth()
	{
		EditText cardNumberEditText = (EditText) findViewById(R.id.cardNumberEditText);
		EditText personCountEditText = (EditText) findViewById(R.id.personCountEditText);

		cardNumberEditText.setTextSize(editTextSizeWidth());
		personCountEditText.setTextSize(editTextSizeWidth());

		cardNumberEditText.setMinimumWidth(editTextMinWidth());
		personCountEditText.setMinimumWidth(editTextMinWidth());
	}
}

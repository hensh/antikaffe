package com.hensh.anticaffee;

import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.model.Visit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class SearchVisitActivity extends Activity
{	
	DataStorage storage;
	private int displayWidth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_search_visit);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;
		
		setTextViewSizeWidth();
		setEditTextSizeWidth();
		
		storage = new DataStorage(this);
		
		EditText cardNumberEditText = (EditText) findViewById(R.id.finishVisitCardNumberEditText);
		cardNumberEditText.setOnKeyListener(new OnKeyListener()
		{
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER)
				{
					int cardNumber = Integer.parseInt(((EditText)v).getText().toString());
					finishVisit(cardNumber);
					return true;
				}
				return false;
			}
		});
	}
	
	private boolean finishVisit(int cardNumber)
	{		
		Visit visit = storage.getLastVisit(cardNumber);
		if (visit == null || !visit.isActive())
		{
			return false;
		}
		
		Intent billIntent = new Intent(this, CalculateBillActivity.class);
		billIntent.putExtra(UIConsts.ExtrasVisitID, visit.getId());
		startActivity(billIntent);
		finish();
		return true;
	}
	
	
	private int textViewSizeWidth()
	{
		return displayWidth / 26;
	}
	
	private int editTextSizeWidth()
	{
		return displayWidth / 16;
	}
	
	private int editTextMinWidth()
	{
		return displayWidth / 9;
	}
	
	private void setTextViewSizeWidth()
	{
		TextView finishVisitCardNumberTextView = (TextView) findViewById(R.id.finishVisitCardNumberTextView);
	
		finishVisitCardNumberTextView.setTextSize(textViewSizeWidth());
	}
	
	private void setEditTextSizeWidth()
	{
		EditText finishVisitCardNumberEditText = (EditText) findViewById(R.id.finishVisitCardNumberEditText);
		
		finishVisitCardNumberEditText.setTextSize(editTextSizeWidth());
		
		finishVisitCardNumberEditText.setMinimumWidth(editTextMinWidth());
	}
}

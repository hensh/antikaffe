package com.hensh.anticaffee.widget;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.hensh.anticaffee.R;

public abstract class BillRequestWidgetActivity extends Activity
{
	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, BillRequestWidgetModel model)
	{
		int widgetID = model.getWidgetId();
		
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.bill_request_widget_view);
		updateButtonClicks(remoteViews, context, widgetID);
		remoteViews.setTextViewText(R.id.cardNumberTextView, model.getCardNumber());
		
		appWidgetManager.updateAppWidget(widgetID, remoteViews);
	}
	
	private static void updateButtonClicks(RemoteViews remoteViews, Context context, int widgetId)
	{
		setPendingIntent(context, remoteViews, "0", R.id.button0, widgetId);
		setPendingIntent(context, remoteViews, "1", R.id.button1, widgetId);
		setPendingIntent(context, remoteViews, "2", R.id.button2, widgetId);
		setPendingIntent(context, remoteViews, "3", R.id.button3, widgetId);
		setPendingIntent(context, remoteViews, "4", R.id.button4, widgetId);
		setPendingIntent(context, remoteViews, "5", R.id.button5, widgetId);
		setPendingIntent(context, remoteViews, "6", R.id.button6, widgetId);
		setPendingIntent(context, remoteViews, "7", R.id.button7, widgetId);
		setPendingIntent(context, remoteViews, "8", R.id.button8, widgetId);
		setPendingIntent(context, remoteViews, "9", R.id.button9, widgetId);
		
		Intent clearIntent = new Intent(context, BillRequestWidgetProvider.class);
		clearIntent.setAction(WidgetConsts.BillRequestClearPressed + "_" + widgetId);
		PendingIntent clearPendingIntent = PendingIntent.getBroadcast(context, 0, clearIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setOnClickPendingIntent(R.id.buttonC, clearPendingIntent);
		
		Intent enterIntent = new Intent(context, BillRequestWidgetProvider.class);
		enterIntent.setAction(WidgetConsts.BillRequestEnterPressed + "_" + widgetId);
		PendingIntent enterPendingIntent = PendingIntent.getBroadcast(context, 0, enterIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setOnClickPendingIntent(R.id.buttonEnter, enterPendingIntent);
	}
	
	private static void setPendingIntent(Context context, RemoteViews remoteViews, String figure, int buttonId, int widgetId)
	{
		Intent intent = new Intent(context, BillRequestWidgetProvider.class);
		intent.setAction(WidgetConsts.BillRequestButtonPressed + "_" + figure + "_" + widgetId);
		intent.putExtra(WidgetConsts.FigurePressedExtra, figure);
		
		PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setOnClickPendingIntent(buttonId, actionPendingIntent);
	}
}

package com.hensh.anticaffee.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.widget.RemoteViews;

import com.hensh.anticaffee.CalculateBillActivity;
import com.hensh.anticaffee.CreateNewVisitActivity;
import com.hensh.anticaffee.R;
import com.hensh.anticaffee.SearchVisitActivity;
import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.db.AnticaffeeProviderMetaData.VisitsTableMetaData;
import com.hensh.anticaffee.model.Visit;

public class MainTableWidgetProvider extends AppWidgetProvider
{
	private static final String ActionListClick = "com.hensh.anticaffe.widget/listclick";
	
	private ContentObserver observer = null;
	
	@Override
	public void onUpdate(final Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
	{
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.main_table_widget);
		
		Intent newVisitIntent = new Intent(context, CreateNewVisitActivity.class);
		PendingIntent newVisitBroadcast = PendingIntent.getActivity(context, 0, newVisitIntent, 0);
		remoteViews.setOnClickPendingIntent(R.id.newVisitButton, newVisitBroadcast);
		
		Intent searchVisitIntent = new Intent(context, SearchVisitActivity.class);
		PendingIntent searchBroadCast = PendingIntent.getActivity(context, 0, searchVisitIntent, 0);
		remoteViews.setOnClickPendingIntent(R.id.searchVisitButton, searchBroadCast);
		
		Intent visitsBindIntent = new Intent(context, VisitsRemoteViewService.class);
		remoteViews.setRemoteAdapter(R.id.visitsListView, visitsBindIntent);
		
		final Intent serviceIntent = new Intent(context, VisitsRemoteViewService.class);
		remoteViews.setRemoteAdapter(R.id.visitsListView, serviceIntent);
		
		Intent onListClickIntent = new Intent(context, MainTableWidgetProvider.class);
		onListClickIntent.setAction(ActionListClick);
		onListClickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		onListClickIntent.setData(Uri.parse(onListClickIntent.toUri(Intent.URI_INTENT_SCHEME)));
		
		final PendingIntent onListClickPendingIntent = PendingIntent.getBroadcast(context, 0, onListClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setPendingIntentTemplate(R.id.visitsListView, onListClickPendingIntent);
		
		appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
		
		if (observer != null)
		{
			context.getContentResolver().unregisterContentObserver(observer);
		}
		observer = new ContentObserver(new Handler())
		{
			@Override
			public void onChange(boolean selfChange)
			{
				super.onChange(selfChange);
				
				AppWidgetManager manager = AppWidgetManager.getInstance(context);
				int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(context, MainTableWidgetProvider.class));
				manager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.visitsListView);
			}
		};
		context.getContentResolver().registerContentObserver(VisitsTableMetaData.CONTENT_URI, true, observer);
	}
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		super.onReceive(context, intent);
		
		String action = intent.getAction(); 
		if (action.equals(ActionListClick))
		{
			long visitId = intent.getLongExtra(UIConsts.ExtrasVisitID, Visit.UndefinedID);
			if (visitId != Visit.UndefinedID)
			{
				Intent billIntent = new Intent(context, CalculateBillActivity.class);
				billIntent.putExtra(UIConsts.ExtrasVisitID, visitId);
				billIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(billIntent);
			}
		}
	}
}

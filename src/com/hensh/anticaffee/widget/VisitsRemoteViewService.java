package com.hensh.anticaffee.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class VisitsRemoteViewService extends RemoteViewsService
{

	@Override
	public RemoteViewsFactory onGetViewFactory(Intent intent)
	{
		return new VisitsRemoteViewFactory(getApplicationContext());
	}

}

package com.hensh.anticaffee.widget;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

public class BillRequestWidgetModel extends WidgetPrefsModel
{
	private static final String WidgetPrefsName = "com.hensh.anticaffee.widget.bill_request_prefs_name";
	private static final String CardNumberKey = "CardNumberKey"; 
	
	private String cardNumber = "";
	
	public String getCardNumber()
	{
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}
	
	public BillRequestWidgetModel(int widgetId)
	{
		super(widgetId);
	}

	@Override
	public String getPrefsName()
	{
		return WidgetPrefsName;
	}
	
	@Override
	public Map<String, String> getKeyValueMap()
	{
		Map<String, String> keyValueMap = new HashMap<String, String>();
		keyValueMap.put(CardNumberKey, cardNumber);
		return keyValueMap;
	}

	@Override
	public void setValueForPref(String key, String value)
	{
		if (key.equals(getStoredKeyForFieldName(CardNumberKey)))
		{
			cardNumber = value;
		}		
	}
	
	public static void clearAllPreferences(Context context)
	{
		WidgetPrefsModel.clearAllPreferences(context, WidgetPrefsName);
	}
	
	public static BillRequestWidgetModel retreiveModel(Context context, int widgetId)
	{
		BillRequestWidgetModel model = new BillRequestWidgetModel(widgetId);
		model.retrieveModel(context);
		return model;
	}
}

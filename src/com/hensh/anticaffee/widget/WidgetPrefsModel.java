package com.hensh.anticaffee.widget;

import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class WidgetPrefsModel
{	
	protected final int widgetID;
	public abstract String getPrefsName();
	public abstract void setValueForPref(String key, String value);
	public Map<String, String> getKeyValueMap() 
	{
		return null; 
	};
	
	public int getWidgetId()
	{
		return widgetID;
	}
	
	public WidgetPrefsModel(int widgetId)
	{
		this.widgetID = widgetId;
	}
	
	protected boolean isItMyPref(String key)
	{
		return key.endsWith(String.valueOf(widgetID));
	}
	
	protected String getStoredKeyForFieldName(String fieldName)
	{
		return fieldName + "_" + widgetID;
	}
	
	public boolean savePreferences(Context context)
	{
		Map<String, String> keyValueMap = getKeyValueMap();
		if (keyValueMap == null)
		{
			return true;
		}
		SharedPreferences.Editor editor = context.getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE).edit();
		for (String key : keyValueMap.keySet())
		{
			savePreference(editor, key, keyValueMap.get(key));
		}
		return editor.commit();
	}
	
	private void savePreference(SharedPreferences.Editor editor, String key, String value)
	{
		String storedKey = getStoredKeyForFieldName(key);
		editor.putString(storedKey, value);
	}
	
	public boolean removePreferences(Context context)
	{
		Map<String, String> keyValueMap = getKeyValueMap();
		if (keyValueMap == null)
		{
			return true;
		}
		SharedPreferences.Editor editor = context.getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE).edit();
		for (String key : keyValueMap.keySet())
		{
			removePreference(editor, key);
		}
		return editor.commit();
	}
	
	private void removePreference(SharedPreferences.Editor editor, String key)
	{
		String storedKey = getStoredKeyForFieldName(key);
		editor.remove(storedKey);
	}
	
	public static boolean clearAllPreferences(Context context, String preferencesName)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE).edit();
		editor.clear();
		return editor.commit();
	}
	
	public boolean retrieveModel(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE);
		Map<String, ?> keyValuePairs = preferences.getAll();
		boolean prefFound = false;
		for (String key : keyValuePairs.keySet())
		{
			if (isItMyPref(key))
			{
				String value = (String) keyValuePairs.get(key);
				setValueForPref(key, value);
				prefFound = true;
			}
		}
		return prefFound;
	}
}

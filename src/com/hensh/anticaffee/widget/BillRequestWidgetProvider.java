package com.hensh.anticaffee.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.hensh.anticaffee.CalculateBillActivity;
import com.hensh.anticaffee.R;
import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.db.DataStorage;
import com.hensh.anticaffee.model.Visit;

public class BillRequestWidgetProvider extends AppWidgetProvider
{	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
	{
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for (int widgetId : appWidgetIds)
		{
			BillRequestWidgetModel model = BillRequestWidgetModel.retreiveModel(context, widgetId);
			BillRequestWidgetActivity.updateAppWidget(context, appWidgetManager, model);
		}
	}
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		super.onReceive(context, intent);
		
		String newCardNumber = "";
		String action = intent.getAction();
		BillRequestWidgetModel model = null;
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		
		if (action.startsWith(WidgetConsts.BillRequestEnterPressed))
		{
			model = getModel(context, action);
			showBill(context, model);
		} 
		else if (action.startsWith(WidgetConsts.BillRequestButtonPressed))
		{
			model = getModel(context, action);
			if (model != null)
			{
				newCardNumber = model.getCardNumber() + intent.getExtras().getString(WidgetConsts.FigurePressedExtra, "");
			}
		}
		else if (action.startsWith(WidgetConsts.BillRequestClearPressed))
		{
			model = getModel(context, action);
		}
		if (model != null)
		{
			model.setCardNumber(newCardNumber);
			model.savePreferences(context);
			BillRequestWidgetActivity.updateAppWidget(context, appWidgetManager, model);
		}
	}
	
	private void showBill(Context context, BillRequestWidgetModel model)
	{
		if (model == null)
		{
			return;
		}
		DataStorage storage = new DataStorage(context);
		Visit visit = storage.getLastVisit(Integer.parseInt(model.getCardNumber()));
		if (visit == null)
		{
			Toast.makeText(context, context.getString(R.string.can_not_find_card_number) + " " + model.getCardNumber(), Toast.LENGTH_SHORT).show();
			return;
		}
		long visitId = visit.getId();
		
		Intent billIntent = new Intent(context, CalculateBillActivity.class);
		billIntent.putExtra(UIConsts.ExtrasVisitID, visitId);
		billIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(billIntent);
	}
	
	private BillRequestWidgetModel getModel(Context context, String action)
	{
		int widgetId = cutWidgetIdFrom(action);
		if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID)
		{
			return null;
		}
		return BillRequestWidgetModel.retreiveModel(context, widgetId);
	}
	
	private int cutWidgetIdFrom(String broadcast)
	{
		try
		{
			String result = broadcast.substring(broadcast.lastIndexOf('_')+1);
			return Integer.parseInt(result);
		}
		catch (NumberFormatException e)
		{
			return AppWidgetManager.INVALID_APPWIDGET_ID;
		}
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds)
	{
		super.onDeleted(context, appWidgetIds);
		
		for (int widgetId : appWidgetIds)
		{
			BillRequestWidgetModel model = BillRequestWidgetModel.retreiveModel(context, widgetId);
			model.removePreferences(context);
		}
	}
	
	@Override
	public void onEnabled(Context context)
	{
		super.onEnabled(context);
		
		BillRequestWidgetModel.clearAllPreferences(context);
	}
	
	@Override
	public void onDisabled(Context context)
	{
		super.onDisabled(context);
		
		BillRequestWidgetModel.clearAllPreferences(context);
	}
}

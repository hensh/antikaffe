package com.hensh.anticaffee.widget;

import java.text.DateFormat;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import com.hensh.anticaffee.R;
import com.hensh.anticaffee.UI.UIConsts;
import com.hensh.anticaffee.db.VisitsDataSource;
import com.hensh.anticaffee.model.Visit;

public class VisitsRemoteViewFactory implements RemoteViewsFactory
{
	private final Context context;
	private final DateFormat dateFormat;
	private VisitsDataSource dataSource = null;
	
	public VisitsRemoteViewFactory(Context context)
	{
		this.context = context;
		dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
	}
	
	@Override
	public int getCount()
	{
		return dataSource.getCount();
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public RemoteViews getLoadingView()
	{
		return null;
	}

	@Override
	public RemoteViews getViewAt(int position)
	{
		RemoteViews listItemRemoteView = new RemoteViews(context.getPackageName(), R.layout.visit_item_view);
		
		listItemRemoteView.setTextViewText(R.id.cardNumberTextView, String.valueOf(position));
		
		Visit visit = dataSource.getVisit(position);
		listItemRemoteView.setTextViewText(R.id.cardNumberTextView, String.valueOf(visit.getCardNumber()));
		listItemRemoteView.setTextViewText(R.id.personsCountTextView, String.valueOf(visit.getPersonsCount()));
		listItemRemoteView.setTextViewText(R.id.enterDateTextView, dateFormat.format(visit.getEnterDate()));
		
		Intent onClickIntent = new Intent();
		onClickIntent.putExtra(UIConsts.ExtrasVisitID, visit.getId());
		listItemRemoteView.setOnClickFillInIntent(R.id.visitItemId, onClickIntent);
		
		return listItemRemoteView;
	}

	@Override
	public int getViewTypeCount()
	{
		return 1;
	}

	@Override
	public boolean hasStableIds()
	{
		return true;
	}

	@Override
	public void onDataSetChanged()
	{
		dataSource = new VisitsDataSource(VisitsDataSource.ActiveOnly, context.getContentResolver());
	}

	@Override
	public void onCreate()
	{
		dataSource = new VisitsDataSource(VisitsDataSource.ActiveOnly, context.getContentResolver());
	}

	@Override
	public void onDestroy()
	{
		dataSource = null;
	}
}

package com.hensh.anticaffee.widget;

public abstract class WidgetConsts
{
	static final String BillRequestClearPressed = "com.hensh.anticaffee.widget.billrequest.clear_button_pressed";
	static final String BillRequestEnterPressed = "com.hensh.anticaffee.widget.billrequest.enter_button_pressed";
	static final String BillRequestButtonPressed = "com.hensh.anticaffee.widget.billrequest.number_button_pressed";
	static final String FigurePressedExtra = "FigurePressedExtra";
}

package com.hensh.anticaffee;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

//TODO ������� ����� ������ ���������
public class CalculatorPreferencesActivity extends Activity
{
	AppPrefs prefs = null;
	private int displayWidth;
	private int displayHeight;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator_preferences);
		prefs = new AppPrefs(this);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;
		displayHeight = metrics.heightPixels;
		
		setTextViewSizeWidth();
		setEditTextSizeWidth();
		setSavePrefTextSize();
	}
	

	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		
		setTextViewSizeHeight();
		setEditTextSizeHeight();
		
		EditText firstHourCostEditText = (EditText) findViewById(R.id.firstHourCostEditText);
		String firstHourCost = firstHourCostEditText.getText().toString();
		
		EditText secondHourCostEditText = (EditText) findViewById(R.id.secondHourCostEditText);
		String secondHourCost = secondHourCostEditText.getText().toString();
		
		setContentView(R.layout.activity_calculator_preferences);
		
		setTextViewSizeWidth();
		setEditTextSizeWidth();
		setSavePrefTextSize();
		
		firstHourCostEditText.setText(firstHourCost);
		secondHourCostEditText.setText(secondHourCost);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		EditText firsHourCost = (EditText) findViewById(R.id.firstHourCostEditText);
		firsHourCost.setText(String.valueOf(prefs.getFirstHourCost()));
		
		EditText secondHourCost = (EditText) findViewById(R.id.secondHourCostEditText);
		secondHourCost.setText(String.valueOf(prefs.getSecondHourCost()));
	}
	
	public void saveAction(View view)
	{		
		EditText firsHourCostEditText = (EditText) findViewById(R.id.firstHourCostEditText);
		int firstHourCost = Integer.parseInt(firsHourCostEditText.getText().toString());
		
		EditText secondHourCostEditText = (EditText) findViewById(R.id.secondHourCostEditText);
		int secondHourCost = Integer.parseInt(secondHourCostEditText.getText().toString());
		
		prefs.setCalculatorParams(firstHourCost, secondHourCost);
		finish();
	}
	
	private int textViewSizeWidth()
	{
		return displayWidth / 26;
	}
	
	private int editTextSizeWidth()
	{
		return displayWidth / 16;
	}
	
	private int editTextMinWidth()
	{
		return displayWidth / 9;
	}
	
	private int savePrefTextSize()
	{
		return displayWidth / 32;
	}
	
	private int textViewSizeHeight()
	{
		return displayHeight / 16;
	}
	
	private double editTextSizeHeight()
	{
		return displayHeight / 9.6;
	}
	
	private void setTextViewSizeWidth()
	{
		TextView firstHourCostTextView = (TextView) findViewById(R.id.firstHourCostTextView);
		TextView nextHourCostTextView = (TextView) findViewById(R.id.nextHourCostTextView);
		
		firstHourCostTextView.setTextSize(textViewSizeWidth());
		nextHourCostTextView.setTextSize(textViewSizeWidth());
	}
	
	private void setEditTextSizeWidth()
	{
		EditText firstHourCostEditText = (EditText) findViewById(R.id.firstHourCostEditText);
		EditText secondHourCostEditText = (EditText) findViewById(R.id.secondHourCostEditText);
		
		firstHourCostEditText.setTextSize(editTextSizeWidth());
		secondHourCostEditText.setTextSize(editTextSizeWidth());
		
		firstHourCostEditText.setMinimumWidth(editTextMinWidth());
		secondHourCostEditText.setMinimumWidth(editTextMinWidth());
	}
	
	private void setTextViewSizeHeight()
	{
		TextView firstHourCostTextView = (TextView) findViewById(R.id.firstHourCostTextView);
		TextView nextHourCostTextView = (TextView) findViewById(R.id.nextHourCostTextView);
		
		firstHourCostTextView.setTextSize(textViewSizeHeight());
		nextHourCostTextView.setTextSize(textViewSizeHeight());
	}
	
	private void setEditTextSizeHeight()
	{
		EditText firstHourCostEditText = (EditText) findViewById(R.id.firstHourCostEditText);
		EditText secondHourCostEditText = (EditText) findViewById(R.id.secondHourCostEditText);
		
		firstHourCostEditText.setTextSize((float) editTextSizeHeight());
		secondHourCostEditText.setTextSize((float) editTextSizeHeight());
		
		firstHourCostEditText.setMinimumWidth(editTextMinWidth());
		secondHourCostEditText.setMinimumWidth(editTextMinWidth());
	}
	
	private void setSavePrefTextSize() 
	{
		CheckBox roundingCheckBox = (CheckBox) findViewById(R.id.roundingCheckBox);
		Button savePrefButton = (Button) findViewById(R.id.saveCalculatorPreferences);
		
		roundingCheckBox.setTextSize(savePrefTextSize());
		savePrefButton.setTextSize(savePrefTextSize());
	}
}

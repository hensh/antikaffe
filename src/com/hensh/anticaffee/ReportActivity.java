package com.hensh.anticaffee;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.hensh.anticaffee.db.Report;
import com.hensh.anticaffee.db.VisitsDataSource;

public class ReportActivity extends Activity
{
	private int displayWidth;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_report);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayWidth = metrics.widthPixels;

		setButtonTextSize();
		setTextViewSizeWidth();

		setDateRange(Calendar.DATE, -1);
	}

	public void createReportAction(View view)
	{
		Date startDate = getStartDate();
		Date endDate = getEndDate();

		VisitsDataSource visitsDataSource = null;
		try
		{
			String fileName = Report.getReportName(startDate, endDate, this);
			fileName = fileName.replaceAll("[\\\\/:*?\"<>|]+", "_") + ".xls";

			File file = new File(this.getExternalFilesDir(null), fileName);
			file.delete();
			if (!file.createNewFile())
			{
				Toast.makeText(this, "Try again. Can not create file: " + fileName, Toast.LENGTH_LONG).show();
				return;
			}

			visitsDataSource = new VisitsDataSource(startDate, endDate, VisitsDataSource.ClosedOnly, this.getContentResolver());
			Report.export(file, this, visitsDataSource);

			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			String address = getString(R.string.default_mail);
			String subject = getString(R.string.anticafe_report);
			String emailtext = "";

			emailIntent.setType("plain/text");

			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]
			{
				address
			});
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, emailtext);

			this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			finish();

		}
		catch (Throwable t)
		{
			Toast.makeText(this, "Request failed: " + t.toString(), Toast.LENGTH_LONG).show();
		}
		finally
		{
			if (visitsDataSource != null)
			{
				visitsDataSource.close();
			}
		}
	}

	public void setDayRangeAction(View view)
	{
		setDateRange(Calendar.DATE, -1);
	}

	public void setWeekRangeAction(View view)
	{
		setDateRange(Calendar.DATE, -7);
	}

	public void setMonthRangeAction(View view)
	{
		setDateRange(Calendar.MONTH, -1);
	}

	private void setDateRange(int calendarField, int value)
	{
		DatePicker startDatePicker = (DatePicker) findViewById(R.id.startDatePicker);
		DatePicker endDatePicker = (DatePicker) findViewById(R.id.endDatePicker);

		Calendar calendar = Calendar.getInstance();
		endDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		calendar.add(calendarField, value);
		startDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
	}

	private Date getStartDate()
	{
		DatePicker startDatePicker = (DatePicker) findViewById(R.id.startDatePicker);
		return getDateFrom(startDatePicker);
	}

	private Date getEndDate()
	{
		DatePicker endDatePicker = (DatePicker) findViewById(R.id.endDatePicker);
		return getDateFrom(endDatePicker);
	}

	private Date getDateFrom(DatePicker datePicker)
	{
		int day = datePicker.getDayOfMonth();
		int month = datePicker.getMonth();
		int year = datePicker.getYear();
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);

		return calendar.getTime();
	}

	private int textViewSizeWidth()
	{
		return displayWidth / 36;
	}

	private int buttonTextSize()
	{
		return displayWidth / 32;
	}

	private int reportButtonSize()
	{
		return displayWidth / 26;
	}

	private void setTextViewSizeWidth()
	{
		TextView reportStartDateTextView = (TextView) findViewById(R.id.reportStartDateTextView);
		TextView reportEndDateTextView = (TextView) findViewById(R.id.reportEndDateTextView);

		reportEndDateTextView.setTextSize(textViewSizeWidth());
		reportStartDateTextView.setTextSize(textViewSizeWidth());
	}

	private void setButtonTextSize()
	{
		Button reportForDayButton = (Button) findViewById(R.id.reportForDayButton);
		Button reportForWeekButton = (Button) findViewById(R.id.reportForWeekButton);
		Button reportForMonthButton = (Button) findViewById(R.id.reportForMonthButton);
		Button reportCreateReportButton = (Button) findViewById(R.id.reportCreateReportButton);

		reportForDayButton.setTextSize(buttonTextSize());
		reportForWeekButton.setTextSize(buttonTextSize());
		reportForMonthButton.setTextSize(buttonTextSize());
		reportCreateReportButton.setTextSize(reportButtonSize());
	}
}
